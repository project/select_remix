********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Select Remix Module
Author: Robert Castelo <www.codepositive.com>
Drupal: 6.x
********************************************************************
DESCRIPTION:

Hide specific options of a CCK select field for particular content type(s).


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes. 



********************************************************************
AUTHOR CONTACT

   
- Comission New Features:
   http://drupal.org/user/3555/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT


